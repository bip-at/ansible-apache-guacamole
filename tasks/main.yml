---
# Title: ansible-role-guacamole
#
# Author: Luc Rutten
# File tasks/main2.yml
#
# Description:
#   Apache Guacamole is a clientless remote desktop gateway.

- name: "install mariadb"
  import_tasks: mariadb.yml
  when: guac_mariadb_state != 'disabled'

- name: "create ansible and guacamole directory structure"
  file:
    path: "{{ item }}"
    state: directory
  with_items:
    - /opt/ansible
    - /opt/ansible/projects
    - /opt/ansible/projects/guacamole
    - /opt/ansible/projects/guacamole/scripts
    - /opt/ansible/projects/guacamole/resources
    - /etc/guacamole
    - /etc/guacamole/lib
    - /etc/guacamole/extensions
    - /usr/share/tomcat8
    - /usr/share/tomcat8/.guacamole
    
- name: "install packages"
  apt:
    name: "{{ packages }}"
    update_cache: yes
    state: present
  vars:
    packages:
    - build-essential 
    - gcc
    - g++
    - ghostscript
    - jq
    - haveged
    - openjdk-8-jre-headless
    - tomcat8    
    # Required dependencies
    - libcairo2-dev
    - libjpeg-turbo8-dev    
    - libpng-dev
    - libtool-bin
    - libossp-uuid-dev
    # Optional dependencies
    - libavcodec-dev
    - libavformat-dev
    - libavutil-dev
    - libswscale-dev
    - freerdp2-dev
    - libpango1.0-dev
    - libssh2-1-dev
    - libtelnet-dev
    - libvncserver-dev
    - libwebsockets-dev
    - libpulse-dev
    - libssl-dev
    - libvorbis-dev
    - libwebp-dev
    - mariadb-client
  
- name: "stop tomcat service"
  systemd:
    name: tomcat8
    state: stopped

- name: "transfer templates files to the specified directories"
  template:
    src: "{{ item.src }}"
    dest: "{{ item.dest }}"
    backup: yes
  with_items:
    - { src: 'tomcat8.j2', dest: '/etc/default/tomcat8' }
    - { src: 'guacamole.properties.j2', dest: '/etc/guacamole/guacamole.properties' }
    - { src: 'gmt.cnf.j2', dest: '/etc/mysql/conf.d/gmt.cnf' }

- name: "create symlink from /etc/guacamole/guacamole.properties to /usr/share/tomcat8/.guacamole/guacamole.properties"
  file:
    src: /etc/guacamole/guacamole.properties
    dest: /usr/share/tomcat8/.guacamole/guacamole.properties
    state: link    
    
- name: "download guacamole server and client files"
  get_url:
    url: "{{ item.src }}"
    dest: "{{ item.dst }}"
  with_items:
    - { src: '{{ guac_server_src }}', dst: '{{ guac_server_dir }}' }
    - { src: '{{ guac_client_src }}', dst: '{{ guac_client_dst }}' }

- name: "unarchive guacamole server files"
  unarchive: 
    src: "{{ guac_server_dir }}/{{ guac_server_tar }}"
    dest: "{{ guac_server_dir }}"
    remote_src: yes

- name: "create initial guacamole mysql database"
  mysql_db:
    name: "{{ guac_dbname }}"
    encoding: utf8
    state: present
    login_user: "{{ mysql_root_account }}"
    login_password: "{{ mysql_root_password }}"

- name: "create guacamole mysql database user"
  mysql_user:
    name: "{{ guac_dbuser }}"
    password: "{{ guac_dbpass }}"
    priv: "{{ guac_dbname }}.*:ALL,GRANT"
    state: present
    login_user: "{{ mysql_root_account }}"
    login_password: "{{ mysql_root_password }}"

- name: "running ./configure for guacamole {{ guac_server_dir_unpacked }}"
  command: '"{{ item }}" chdir="{{ guac_server_dir_unpacked }}/" --with-init-dir=/etc/init.d'
  with_items:
    - ./configure

- name: "running make for guacamole {{ guac_server_dir_unpacked }}"
  command: '"{{ item }}" chdir="{{ guac_server_dir_unpacked }}/"'
  with_items:
    - make

- name: "running make install for guacamole"
  command: 'make install chdir="{{ guac_server_dir_unpacked }}/"'

- name: "run ldconfig for guacamole"
  command: 'ldconfig'

- name: prepare extension list
  set_fact:
    guac_extensions:
      - { download: '{{ guac_mysql_conn_src }}', src: '{{ guac_mysql_conn_file }}', dest: '{{ guac_mysql_conn_dst }}', state: 'enabled' }
      - { download: '{{ guac_ldap_conn_src }}', src: '{{ guac_ldap_conn_file }}', dest: '{{ guac_ldap_conn_dst }}', state: '{{ guac_ldap_auth_state }}' }
      - { download: '{{ guac_totp_auth_src }}', src: '{{ guac_totp_auth_file }}', dest: '{{ guac_totp_auth_dst }}', state: '{{ guac_totp_auth_state}}' }
      - { download: '{{ guac_sso_auth_src }}', src: '{{ guac_sso_auth_file }}', dest: '{{ guac_sso_auth_dst }}', state: '{{ guac_sso_auth_state }}'}

- name: "download and unarchive files connectors"
  unarchive:
    src: "{{ item.download }}"
    dest: "{{ guac_extension_dir }}/"
    remote_src: yes
  with_items: "{{ guac_extensions }}"

- name: "disable extensions"
  file:
    path: "{{ item.dest }}"
    state: absent 
  when: item.state == 'disabled'
  with_items: "{{ guac_extensions }}"

- name: "enable extensions"
  copy:
    src: "{{ item.src }}"
    dest: "{{ item.dest }}"
    remote_src: yes
  when: item.state != 'disabled'
  with_items: "{{ guac_extensions }}"

- name: "import mysql database schemas"
  mysql_db:
    state: import
    name: "{{ guac_dbname }}"
    login_user: "{{ guac_dbuser }}"
    login_password: "{{ guac_dbpass }}"
    target: "{{ item }}"
  with_items:
    - "{{ guac_mysql_conn_src_schema }}/001-create-schema.sql"
    - "{{ guac_mysql_conn_src_schema }}/002-create-admin-user.sql"
  ignore_errors: yes    
    
- name: "download and unarchive files connectors"      
  apt:
    deb: "{{ guac_mysql_conn_java_src }}"

- name: "copy from /usr/share/java/mysql-connector-java-{{ guac_mysql_conn_version }}.jar to {{ guac_mysql_conn_java_dst }}" 
  copy:
    src: /usr/share/java/mysql-connector-java-{{ guac_mysql_conn_version }}.jar
    dest: "{{ guac_mysql_conn_java_dst }}/mysql-connector-java-{{ guac_mysql_conn_version }}.jar"
    remote_src: yes

- name: "allow all traffic on port 8080/tcp "
  ufw: 
    rule: allow
    port: "8080"
    proto: tcp

- name: "restart guacen tomcat8 services"
  systemd:
    name: "{{ item }}"
    state: restarted
    enabled: yes
  with_items:   
    - guacd
    - tomcat8

- name: "import branding"
  import_tasks: branding.yml

- name: "import apache2"
  import_tasks: apache2.yml
  when: guac_apache2_state != 'disabled'
